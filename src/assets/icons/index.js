import IconLogo from './logo.svg'
import IconLogoBlack from './logoBlack.svg'
import IconLogoWhite from './logoWhite.svg'
import IconMenu from './menu.svg'
import IconNotif from './notif.svg'
import IconPlus from './plus.svg'
import BackgroundCard from './backgroudCard.svg'
import IconBitcoin from './bitcoin.svg'
import IconQrCode from './qrCode.svg'
import Today1 from './today1.svg'
import Today3 from './today3.svg'
import Today2 from './today2.svg'
import IconQrCodeBlack from './qrCodeBlack.svg'
import IconClock from './clock.svg'
import IconWallet from './menuWallet.svg'
import IconExchange from './menuExchange.svg'
import IconReferral from './menuReferral.svg'
import IconBank from './menuBank.svg'
import IconDeposit from './menuDeposit.svg'
import IconProfile from './profile.svg'
import IconFlag from './flag.svg'
import iconSearch from './iconSearch.svg'
import IconBca from './bca.svg'
import IconBni from './bni.svg'
import IconBri from './bri.svg'
import IconMandiri from './mandiri.svg'
import IconSignOut from './signOut.svg'
import IconWithdraw from './withdraw.svg'
import IconWithdrawHome from './withdrawHome.svg'




export {
    IconLogo,
    IconLogoBlack,
    IconLogoWhite,
    IconMenu,
    IconNotif,
    IconPlus,
    BackgroundCard,
    IconBitcoin,
    IconQrCode,
    Today1,
    Today2,
    Today3,
    IconQrCodeBlack,
    IconClock,
    IconWallet,
    IconExchange,
    IconReferral,
    IconBank,
    IconDeposit,
    IconProfile,
    IconFlag,
    iconSearch,
    IconBca,
    IconBni,
    IconBri,
    IconMandiri,
    IconSignOut,
    IconWithdraw,
    IconWithdrawHome
}