import React from "react";
import {
    BrowserRouter,
    Routes,
    Route,
  } from "react-router-dom";
import {DepositePage, ExchangePage, HomePage, LoginPage, ReferralPage, RegisterPage, SplashScreenPage,ExchangePageDetail, BankPage,WithdrawPage, ChoosePayment, ListDeposit, DepositAmount, QrDeposit} from "../pages";

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<SplashScreenPage/>} exact/>
        <Route path="/login" element={<LoginPage/>}/>
        <Route path="/register" element={<RegisterPage/>}/>
        <Route path="/home" element={<HomePage/>}/>
        <Route path="/bank" element={<BankPage/>}/>
        <Route path="/deposit" element={<DepositePage/>}/>
        <Route path="/exchange" element={<ExchangePage/>}/>
        <Route path="/exchange/:id" element={<ExchangePageDetail/>}/>
        <Route path="/referral" element={<ReferralPage/>}/>
        <Route path="/withdraw" element={<WithdrawPage/>}/>
        <Route path="/choosePayment" element={<ChoosePayment/>}/>
        <Route path="/listDeposit" element={<ListDeposit/>}/>
        <Route path="/depositAmount" element={<DepositAmount/>}/>
        <Route path="/qrDeposit" element={<QrDeposit/>}/>
      </Routes>
    </BrowserRouter>
  );
};

export default Router;