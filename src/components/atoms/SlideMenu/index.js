import axios from 'axios'
import React,{useEffect,useState} from 'react'
import { Image } from 'react-bootstrap'
import { slide as Menu } from 'react-burger-menu'
import { useLocation,Link,useNavigate } from 'react-router-dom'
import { IconBank, IconDeposit, IconExchange, IconReferral, IconWallet,IconProfile,IconSignOut, IconWithdraw } from '../../../assets'
import { URLAPI } from '../../../utils/configs'
import styles from './styles.module.css'
import AsyncStorage from '@react-native-async-storage/async-storage';

const SlideMenu = (props) => {
    const [user, setUser]= useState([])
    
    let location = useLocation();
    const navigate = useNavigate();

    useEffect(() => {
      async function fetchData() {
        try {
          const savedUser = await AsyncStorage.getItem("user");
          const currentUser = JSON.parse(savedUser);
          setUser(currentUser);
          AsyncStorage.setItem('username', currentUser.username);
        } catch (error) {
          console.log(error);
        }
      }
      fetchData();
    }, [])
    useEffect(() => {
      async function auth() {
        try {
          const token = await AsyncStorage.getItem('token');
          if(!token){
            navigate('/login')
          }
        } catch (error) {
          console.log(error);
        }
      }
      auth();
    }, [])

    const logout = async () =>{
      axios({
        url: URLAPI +'/logout',
        method: 'get',
        headers: {
          Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        })
        .then(response => {
          console.log(response)
            AsyncStorage.clear("token");
           navigate('/login')
        })
        .catch(error => {
          console.log(error)
            
        });
    }

    return (
      <Menu>
        <Image src={IconProfile} width={45} height={45} />
        <p className={styles.textName}>{user.username}</p>
        <p className={styles.textMember}>Member #1378</p>
        <p className={styles.textView}>View Profile</p>
        <p className={styles.textTitle}>Transactions</p>
        <Link id="wallet" className={location.pathname === '/home' ? styles.menuActive :styles.menuNonActive} to="/home"><Image src={IconWallet} className={styles.iconMenu}/> Wallet</Link>
        <Link id="bank" className={location.pathname === '/bank' ? styles.menuActive :styles.menuNonActive} to="/bank"><Image src={IconBank} className={styles.iconMenu}/> Bank</Link>
        <Link id="deposit" className={location.pathname === '/deposit' ? styles.menuActive :styles.menuNonActive} to="/deposit"><Image src={IconDeposit} className={styles.iconMenu}/>Deposit</Link>
        <Link id="withdraw" className={location.pathname === '/withdraw' ? styles.menuActive :styles.menuNonActive} to="/withdraw"><Image src={IconWithdraw} className={styles.iconMenu}/>Withdraw</Link>
        <Link id="exchange" className={location.pathname === '/exchange' ? styles.menuActive :styles.menuNonActive} to="/exchange"><Image src={IconExchange} className={styles.iconMenu}/>Exchange</Link>
        <Link id="referral" className={location.pathname === '/referral' ? styles.menuActive :styles.menuNonActive} to="/referral"><Image src={IconReferral} className={styles.iconMenu}/>Referral</Link>
        <p className={styles.menuNonActive} style={{cursor:'pointer'}} onClick={logout}><Image src={IconSignOut} className={styles.iconMenu}/>SignOut</p>
      </Menu>
    );
}

export default SlideMenu