import React, { useEffect } from 'react'
import { Container, Image, Row } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import {IconLogo} from '../../assets'
import styles from './styles.module.css'

const SplashScreenPage = () => {
  const navigate = useNavigate();
useEffect(() => {
  setTimeout(() => {
  navigate('/login')
  }, 1000);
}, [])
  return (
    <div className={styles.pages}>
        <Container>
            <Row>
            <Image src={IconLogo}  className={styles.imagesLogo}/>
            </Row>
        </Container>
    </div>
  )
}

export default SplashScreenPage