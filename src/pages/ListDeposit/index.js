import React,{useState,useEffect} from 'react'
import { Container, Image, Row,Col,InputGroup,FormControl } from 'react-bootstrap'
import styles from './styles.module.css'
import { IconLogoWhite, IconNotif,iconSearch, } from '../../assets'
import { Helmet,  SlideMenu } from '../../components'
import axios from 'axios';
import {  URLKLUWYPAY } from '../../utils/configs'
import { useNavigate } from 'react-router-dom'
import AsyncStorage from '@react-native-async-storage/async-storage';


const ListDeposit = () => {
    const [dataBank, setDataBank] = useState([]);
    const navigate = useNavigate();
     const getBank = async () => {
      axios({
        url: URLKLUWYPAY +'/bank',
        method: 'get',
        // headers: {
        //   Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        // },
        })
        .then(response => {
            setDataBank(response.data.data)
        })
        .catch(error => {
          console.log(error)
        });
    }

    useEffect(() => {
        getBank();
    }, [])

    const handleChooseBank = (val) => {
        navigate('/depositAmount', {state: {bankCode : val}})
    }
    
  return (
    <div className={styles.pages}>
      <Helmet title="List Bank Deposit"/>
       <Container className={styles.containerHeader}>
        <Row className={styles.rowHeader}>
          <Col>
          <SlideMenu/>
          </Col>
          <Col>
            <Image src={IconLogoWhite} className={styles.imgLogo}/>
          </Col>
          <Col style={{textAlign:'right'}}>
          <Image src={IconNotif} className={styles.imgMenu}/>
          </Col>
        </Row>
        {/*  */}
        <Row>
          <Col>
          <p className={styles.TextTitle}>List Bank Deposit</p>
          <p className={styles.TextSubTitle}>Choose Bank For Deposit</p>
          </Col>
        </Row>
    </Container>
    <Container className={styles.containerQr}>
      <Row>
      <Col>
      <Row>
          <Col lg={6} md={6} sm={6} className={styles.textCode}>Name </Col>
        </Row>
      <Row className='mt-3 mb-3' style={{maxHeight:'80vh',overflowX:'auto'}}>
        <Col>
        <hr/>
        {dataBank?.map((i,idx)=> {
          return (
            <>
            <Row onClick={()=>handleChooseBank(i?.code)} className={styles.rowBank}>
              <Col><span className={styles.textCode}>{i?.name}</span></Col>
            </Row>
            <hr/>
            </>
          )
        })}
        </Col>
      </Row>
      </Col>
      </Row>
    </Container>
    </div>
  )
}

export default ListDeposit