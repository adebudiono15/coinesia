import React,{useEffect,useState} from 'react'
import { Container, Image, Row,Col,InputGroup,Form,DropdownButton,Dropdown,Button, FormControl } from 'react-bootstrap'
import styles from './styles.module.css'
import { IconLogoWhite, IconNotif, IconQrCodeBlack,IconFlag, IconExchange} from '../../assets'
import { Helmet, Required, SlideMenu } from '../../components'
import axios from 'axios';
import {  Messaege } from '../../utils/helper'
import { URLAPI } from '../../utils/configs'
import Select from 'react-dropdown-select';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Withdraw = () => {
   const [dataBank, setDataBank] = useState([]);
   const [selectBank, setSelectBank] = useState('')
   const [bank, setBank] = useState([])
   const [user, setUser] = useState([])
    const titleDropdown = () => {
        return (
            <>
            <Image src={IconFlag} width={24} height={24} />
            <span className={styles.textIDR}>IDR</span>
            </>
        )
    }
    const titleDropdownBank = () => {
        return (
            <>
            <span className={styles.textChoseBank}>Choose Bank</span>
            </>
        )
    }

   
    
    const getBank = async () => {
      axios({
        url: URLAPI +`/bank-user/`+ await AsyncStorage.getItem('username'),
        method: 'get',
        headers: {
          Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        })
        .then(response => {
          setDataBank(response.data.data)
          console.log(response.data.data)
        })
        .catch(error => {
          console.log(error)
            
        });
    }

    const items = dataBank.map(item => {
      const data = {};
      data.label = item.name +'-'+ item.number_account;
      data.value = item.name;
      return data;
    });

    useEffect(() => {
      getBank();
  }, [])



  return (
    <div className={styles.pages}>
        <Helmet title="Withdraw"/>
       <Container className={styles.containerHeader}>
        <Row className={styles.rowHeader}>
          <Col>
          <SlideMenu/>
          </Col>
          <Col>
            <Image src={IconLogoWhite} className={styles.imgLogo}/>
          </Col>
          <Col style={{textAlign:'right'}}>
          <Image src={IconNotif} className={styles.imgMenu}/>
          </Col>
        </Row>
        {/*  */}

        <Row>
          <Col>
          <p className={styles.TextTitle}>Withdraw</p>
          </Col>
        </Row>
    </Container>
    <Container className={styles.containerQr}>
        <div
        style={{marginTop:'20px'}}
        />
        <Row>
            <Col>
            <Col>
            <Form.Label className={styles.labelForm}>Amount<Required/></Form.Label>
            <FormControl className={styles.formSearch} placeholder='Amount'  />
           </Col>
      </Col>
        </Row>
        {/*  */}
        <div
        style={{marginTop:'50px'}}
        />
        {/*  */}
        <Row>
            <Col>
            <p className={styles.textCode}>Choose Bank</p>
            <p className={styles.textAddres}>Choose from the following banks you would like to withdraw.</p>
              <Select
              placeholder="Select Bank"
              className={styles.formSearch}
              options={items}
              hideSelectedOptions={false}
              value={bank}
              onChange={(selected) => setSelectBank(selected[0].value)}
              />
            </Col>
        </Row>
        <Row>
            <Col>
            <Button className={styles.btnProceed}>Proceed</Button>
            </Col>
        </Row>
    </Container>
    </div>
  )
}

export default Withdraw