import React from 'react'
import { Container, Image, Row,Col} from 'react-bootstrap'
import styles from './styles.module.css'
import { IconBank, IconLogoWhite, IconNotif, IconWallet} from '../../assets'
import { Helmet, SlideMenu } from '../../components'
import {useNavigate,useLocation} from 'react-router-dom';

const ChoosePayment = (props) => {
  const location = useLocation();
  const navigate = useNavigate();
  const handleBank = () => {
    navigate('/listDeposit')
  }
  return (
    <div className={styles.pages}>
        <Helmet title={location?.state}/>
       <Container className={styles.containerHeader}>
        <Row className={styles.rowHeader}>
          <Col>
          <SlideMenu/>
          </Col>
          <Col>
            <Image src={IconLogoWhite} className={styles.imgLogo}/>
          </Col>
          <Col style={{textAlign:'right'}}>
          <Image src={IconNotif} className={styles.imgMenu}/>
          </Col>
        </Row>
        <Row>
          <Col>
          <p className={styles.TextTitle}>{location?.state}</p>
          <p className={styles.TextSubTitle}>Choose between bank and wallet.</p>
          </Col>
        </Row>
    </Container>
    <Container className={styles.containerQr}>
        <div
        style={{marginTop:'20px'}}
        />
        <Row className='text-center justify-content-center'>
          <Col md={5} sm={5} lg={5}>
            <div className={styles.card} onClick={handleBank}>
            <Image style={{height:32,width:32}} src={IconBank}/>
            </div>
          </Col>
          <Col md={5} sm={5} lg={5}>
            <div className={styles.card}>
            <Image style={{height:32,width:32}} src={IconWallet}/>
            </div>
          </Col>
        </Row>
    </Container>
    </div>
  )
}

export default ChoosePayment