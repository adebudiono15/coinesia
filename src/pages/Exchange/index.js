import React,{useEffect,useState} from 'react'
import { Container, Image, Row,Col,InputGroup,Form,DropdownButton,Dropdown,Button } from 'react-bootstrap'
import styles from './styles.module.css'
import { IconLogoWhite, IconNotif, IconQrCodeBlack,IconFlag, IconExchange} from '../../assets'
import { Helmet, SlideMenu } from '../../components'
import axios from 'axios';
import {  Messaege } from '../../utils/helper'
import { URLAPI } from '../../utils/configs'
import Select from 'react-dropdown-select';
import AsyncStorage from '@react-native-async-storage/async-storage';

const ExchangeDetail = () => {
   const [dataBank, setDataBank] = useState([]);
   const [selectBank, setSelectBank] = useState('')
   const [bank, setBank] = useState([])
   const [user, setUser] = useState([])
    const titleDropdown = () => {
        return (
            <>
            <Image src={IconFlag} width={24} height={24} />
            <span className={styles.textIDR}>IDR</span>
            </>
        )
    }
    const titleDropdownBank = () => {
        return (
            <>
            <span className={styles.textChoseBank}>Choose Bank</span>
            </>
        )
    }

   
    
    const getBank = async () => {
      axios({
        url: URLAPI +`/bank-user/`+ await AsyncStorage.getItem('username'),
        method: 'get',
        headers: {
          Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        })
        .then(response => {
          setDataBank(response.data.data)
          console.log(response.data.data)
        })
        .catch(error => {
          console.log(error)
            
        });
    }

    const items = dataBank.map(item => {
      const data = {};
      data.label = item.name +'-'+ item.number_account;
      data.value = item.name;
      return data;
    });

    useEffect(() => {
      getBank();
  }, [])



  return (
    <div className={styles.pages}>
        <Helmet title="Exchange"/>
       <Container className={styles.containerHeader}>
        <Row className={styles.rowHeader}>
          <Col>
          <SlideMenu/>
          </Col>
          <Col>
            <Image src={IconLogoWhite} className={styles.imgLogo}/>
          </Col>
          <Col style={{textAlign:'right'}}>
          <Image src={IconNotif} className={styles.imgMenu}/>
          </Col>
        </Row>
        {/*  */}

        <Row>
          <Col>
          <p className={styles.TextTitle}>Exchange</p>
          <p className={styles.TextSubTitle}>Scan the QR code below.</p>
          </Col>
        </Row>
    </Container>
    <Container className={styles.containerQr}>
      <Row>
              <Col lg={2} sm={2} md={2} xs={2}>
                  <Image src={IconQrCodeBlack} width={42} height={42} />
              </Col>
                <Col className="text-start"
                style={{marginLeft:'-30px'}}
                > 
                    <p className={styles.textAddres}>Exchange Address</p>
                    <p className={styles.textCode}>X32426ghej378jbf8wtJKLM67</p>
                </Col>
        </Row>

        <div
        style={{marginTop:'20px'}}
        />
        <Row>
            <Col>
            <InputGroup className={styles.inputGroup}>
                <Form.Control className={styles.formControl}  placeholder='You Paid' />
                <DropdownButton
                variant="outline-transparent"
                title={titleDropdown()}
                className={styles.dropdownButton}
                >
                <Dropdown.Item href="#">Action</Dropdown.Item>
                <Dropdown.Item href="#">Another action</Dropdown.Item>
                <Dropdown.Item href="#">Something else here</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item href="#">Separated link</Dropdown.Item>
                </DropdownButton>
            </InputGroup>
      </Col>
        </Row>
        <Row className='mt-3'>
            <Col xs={10}>
                <p className={styles.rate}>Rate of exchange is subject to changes and adjustment in respect of any exchange rate fluctuations.</p>
            </Col>
            <Col lg={1} md={1} sm={1} xs={2}>
            <Image src={IconExchange} />
            </Col>
        </Row>
        <Row>
            <Col>
            <InputGroup className={styles.inputGroup}>
                <Form.Control className={styles.formControl}  placeholder='You Get' />
                <DropdownButton
                variant="outline-transparent"
                title={titleDropdown()}
                className={styles.dropdownButton}
                >
                <Dropdown.Item href="#">Action</Dropdown.Item>
                <Dropdown.Item href="#">Another action</Dropdown.Item>
                <Dropdown.Item href="#">Something else here</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item href="#">Separated link</Dropdown.Item>
                </DropdownButton>
            </InputGroup>
      </Col>
        </Row>
        {/*  */}
        <div
        style={{marginTop:'50px'}}
        />
        {/*  */}
        <Row>
            <Col>
            <p className={styles.textCode}>Choose Bank</p>
            <p className={styles.textAddres}>Choose from the following banks you would like to exchange.</p>
              <Select
              placeholder="Select Bank"
              className={styles.formSearch}
              options={items}
              hideSelectedOptions={false}
              value={bank}
              onChange={(selected) => setSelectBank(selected[0].value)}
              />
            </Col>
        </Row>
        <Row>
            <Col>
            <Button className={styles.btnProceed}>Proceed</Button>
            </Col>
        </Row>
    </Container>
    </div>
  )
}

export default ExchangeDetail