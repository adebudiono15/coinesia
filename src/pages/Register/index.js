import React, { useEffect, useState } from 'react'
import { Col, Container, Row,Image} from 'react-bootstrap'
import styles from './styles.module.css'
import { useNavigate } from 'react-router-dom'
import {ButtonComponent, FormInput, HeaderTitle, Helmet} from '../../components'
import {IconLogoBlack} from '../../assets'
import axios from 'axios';
import {  Messaege } from '../../utils/helper'
import { URLAPI } from '../../utils/configs'
import AsyncStorage from '@react-native-async-storage/async-storage';


const RegisterPage = (props) => {
const [showPassword, setShowPassword] = useState(false)
const [username, setUsername] = useState('')
const [email, setEmail] = useState('')
const [phone, setPhone] = useState('')
const [password, setPassword] = useState('')
const [confirmPassword, setConfirmPassword] = useState('')
const toogleBtn = () => {
        setShowPassword(prevstate => !prevstate)
}

const navigate = useNavigate();
const handleRegister = () => {
  axios({
    url: URLAPI +'/register',
    method: 'post',
    data: {
        username: username,
        email: email,
        phone: phone,
        password: password,
        confrm_password: confirmPassword
    },
    })
    .then(response => {
      Messaege('Succes','Successfully register','success');
       navigate('/login');
    })
    .catch(error => {
        Messaege('Failed','Make sure everything is filled','error');
        
    });
}

const handleLogin = () => {
  navigate('/login')
}

 useEffect(() => {
      async function fetchData() {
        try {
          const token = await AsyncStorage.getItem('token');
          if(!token){
            navigate('/register')
          } else{
            navigate('/home')
          }
        } catch (error) {
          console.log(error);
        }
      }
      fetchData();
    }, [])

  return (
<div className={styles.pages}>
<Helmet title="Register"/>
    <Container>
        <Row>
            <HeaderTitle/>
        </Row>
    </Container>
    <div
    style={{marginTop:50}}
    />
    <Container className={styles.containerWrapper}>
        <Row className={styles.rowWrapper}>
          <Col>
            <Image src={IconLogoBlack} className={styles.imgLogo}/>
          </Col>
        </Row>

        <div className={styles.gapFirstForm}/>

        <Row className={styles.rowWrapper}>
            <FormInput label="Full Name" placeholder="Full Name" type="text" name="username" value={username}   onChange={e => setUsername(e.target.value)}/>
        </Row>
        <div
        style={{marginTop:20}}
        />
        <Row className={styles.rowWrapper}>
            <FormInput label="Email" placeholder="Email" type="email" name="email" value={email}   onChange={e => setEmail(e.target.value)}/>
        </Row>
        <div
        style={{marginTop:20}}
        />
        <Row className={styles.rowWrapper}>
            <FormInput label="Phone" placeholder="+62" type="number" name="phone" value={phone}   onChange={e => setPhone(e.target.value)}/>
        </Row>
        <div
        style={{marginTop:20}}
        />
        <Row className={styles.rowWrapper}>
            <FormInput label="Password" placeholder="Password" type="password" name="password" value={password}   onChange={e => setPassword(e.target.value)}/>
        </Row>
        <div
        style={{marginTop:20}}
        />
        <Row className={styles.rowWrapper}>
            <FormInput label="Confirm Password" placeholder="Compirm Password" type="password" name="confirmPassword" value={confirmPassword}   onChange={e => setConfirmPassword(e.target.value)}/>
        </Row>
        <div
        style={{marginTop:30}}
        />

        <Row className={styles.rowWrapper}>
            <ButtonComponent
             top="Sign Up" 
             bottom="Sign In"
             onClickTop={handleRegister}
             onClickBottom={handleLogin}
             />
        </Row>
        <div
        style={{paddingBottom:30}}
        />
    </Container>
</div>
  )
}

export default RegisterPage