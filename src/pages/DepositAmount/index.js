import React,{useState,useEffect} from 'react'
import { Container, Image, Row,Col,InputGroup,FormControl, Button } from 'react-bootstrap'
import styles from './styles.module.css'
import { IconLogoWhite, IconNotif,iconSearch, } from '../../assets'
import { ButtonComponent, FormInput, Helmet,  SlideMenu } from '../../components'
import axios from 'axios';
import {  URLKLUWYPAY } from '../../utils/configs'
import { useLocation, useNavigate } from 'react-router-dom'
import AsyncStorage from '@react-native-async-storage/async-storage';


const DepositAmount = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [bankCode, setBankCode] = useState(location?.state?.bankCode)
  const [amount, setAmount] = useState('')
  const [cdid, setChid] = useState('')
  const [mid, setMid] = useState('')

  const getDepsiteRoute = async () => {
      axios({
        url: URLKLUWYPAY +'/deposit/route',
        method: 'get',
        headers: {
          type: 'qris',
        },
        })
        .then(response => {
          setChid(response?.data?.data[0]?.chId)
          setMid(response?.data?.data[0]?.mId)
        })
        .catch(error => {
          console.log(error)
        });
    }
    const postDeposit =  () => {
      axios({
        url: URLKLUWYPAY +'/deposit',
        method: 'post',
        data : {
          address: "",
          amount:parseInt(amount),
          bankCode:mid,
          alias: "",
          remarks: "",
          type: "qris",
          addressName: "",
          channelId: cdid,
          refId: 'TesDulu123',
      },
        })
        .then(response => {
          navigate('/qrDeposit',{state : {address:response.data.data.address}})
          // console.log(response.data.data.address)
        })
        .catch(error => {
          console.log(error)
        });
    }

    useEffect(() => {
      getDepsiteRoute();
    }, [])
    
  return (
    <div className={styles.pages}>
      <Helmet title="Deposit"/>
       <Container className={styles.containerHeader}>
        <Row className={styles.rowHeader}>
          <Col>
          <SlideMenu/>
          </Col>
          <Col>
            <Image src={IconLogoWhite} className={styles.imgLogo}/>
          </Col>
          <Col style={{textAlign:'right'}}>
          <Image src={IconNotif} className={styles.imgMenu}/>
          </Col>
        </Row>
        {/*  */}
        <Row>
          <Col>
          <p className={styles.TextTitle}>Deposit</p>
          <p></p>
          </Col>
        </Row>
    </Container>
    <Container className='mt-4'>
      <Row>
        <Col>
        <FormInput label="Amount" placeholder="Amount"  name="amount" value={amount} onChange={e => setAmount(e.target.value)}/>
        <Button className={styles.buttonBlue} onClick={postDeposit}>Proccess</Button>
        </Col>
      </Row>
    </Container>
    </div>
  )
}

export default DepositAmount