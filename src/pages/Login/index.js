import React, { useEffect, useState } from 'react'
import { Col, Container, Row,Image} from 'react-bootstrap'
import styles from './styles.module.css'
import { useNavigate } from 'react-router-dom'
import {ButtonComponent, FormInput, HeaderTitle, Helmet} from '../../components'
import {IconLogoBlack} from '../../assets'
import axios from 'axios';
import {  checkAauth, Messaege } from '../../utils/helper'
import { URLAPI } from '../../utils/configs'
import AsyncStorage from '@react-native-async-storage/async-storage';


const LoginPage = (props) => {
const [showPassword, setShowPassword] = useState(false)
const [email, setEmail] = useState('')
const [password, setPassword] = useState('')

const toogleBtn = () => {
        setShowPassword(prevstate => !prevstate)
}

const navigate = useNavigate();

const handleRegister = async  () => {
 
  navigate('/register')
}

const handleLogin = () => {
  axios({
    url: URLAPI +'/login',
    method: 'post',
    data: {
        email: email,
        password: password,
    },
    })
    .then(response => {
        const firstData = ["token", response.data.data.token];
        const secondData = ["user", JSON.stringify(response.data.data.user)];
        AsyncStorage.multiSet([firstData, secondData]);
       navigate('/home')
    })
    .catch(error => {
        Messaege('Failed','Make sure everything is filled','error');
        
    });

}

  useEffect(() => {
      async function fetchData() {
        try {
          const token = await AsyncStorage.getItem('token');
          if(!token){
            navigate('/login')
          } else{
            navigate('/home')
          }
        } catch (error) {
          console.log(error);
        }
      }
      fetchData();
    }, [])
  return (
<div className={styles.pages}>
  <Helmet title="Login"/>
    <Container>
        <Row>
            <HeaderTitle/>
        </Row>
    </Container>
    <div
    style={{marginTop:50}}
    />
    <Container className={styles.containerWrapper}>
        <Row className={styles.rowWrapper}>
          <Col>
            <Image src={IconLogoBlack} className={styles.imgLogo}/>
          </Col>
        </Row>

        <div className={styles.gapFirstForm}/>

        <Row className={styles.rowWrapper}>
            <FormInput label="Email" placeholder="Email" type="email"  name="email" value={email}   onChange={e => setEmail(e.target.value)} />
        </Row>
        <div
        style={{marginTop:20}}
        />
        <Row className={styles.rowWrapper}>
            <FormInput label="Password" placeholder="Password" type="password" name="password" value={password}   onChange={e => setPassword(e.target.value)}/>
        </Row>
        <div
        style={{marginTop:8}}
        />
        <Row className={styles.rowWrapper}>
          <Col className='text-end'>
            <p className={styles.textForgotPassword}>Forgot Password?</p>
          </Col>
        </Row>

        <Row className={styles.rowWrapper}>
            <ButtonComponent 
            top="Sign In" 
            bottom="Sign Up"
            onClickTop={handleLogin}
            onClickBottom={handleRegister}
            />
        </Row>
        <div 
        style={{paddingBottom:100}}
        />
    </Container>
</div>
  )
}

export default LoginPage