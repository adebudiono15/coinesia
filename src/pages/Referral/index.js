import React from 'react'
import { Container, Image, Row,Col,ProgressBar} from 'react-bootstrap'
import styles from './styles.module.css'
import { IconClock, IconLogoWhite, IconMenu, IconNotif, IconQrCodeBlack} from '../../assets'
import { Helmet, SlideMenu } from '../../components'

const Referral = () =>{
  return (
  <div className={styles.pages}>
      <Helmet title="Referral"/>
    <Container className={styles.containerHeader}>
        <Row className={styles.rowHeader}>
          <Col>
          <SlideMenu/>
          </Col>
          <Col>
            <Image src={IconLogoWhite} className={styles.imgLogo}/>
          </Col>
          <Col style={{textAlign:'right'}}>
          <Image src={IconNotif} className={styles.imgMenu}/>
          </Col>
        </Row>
        {/* Row Card */}
        <Row className={styles.rowCardHeader}>
            <Col>
            <p className={styles.textTitle}>Referral</p>
            <p className={styles.textSubTitle}>Refer this to your friends and get rewards.</p>
            </Col>
        </Row>
        <Row className={styles.rowCard}>
          <Col>
          <Row>
            <Col>
                <p className={styles.textCardTitle}>Bronze</p>
            </Col>
            <Col className='text-end'>
            <span className={styles.badge}>Current Level</span>
            </Col>
          </Row>
          <Row>
            <Col>
            <ProgressBar now={60} variant="success" style={{
                backgroundColor:'#D9D9D9',
                height:10,
                width:'95%',
            }}/>
            </Col>
          </Row>
          <div
          style={{marginTop:10}}
          />
          <Row>
            <p className={styles.textDesc}>Another 5 referrals to upgrade your tier to Silver.</p>
            <p className={styles.textSubDesc}> <Image src={IconClock}/> Make more referrals before 31 May 2022.</p>
          </Row>
          </Col>
        </Row>
        {/* Last Row Card */}
    </Container>
    <Container className={styles.containerQr}>
      <Row className='text-center'>
              <Col>
                  <Image src={IconQrCodeBlack} className={styles.imgMenuQr} />
                  <div
                  style={{marginTop:20}}
                  />
                  <p className={styles.textAddres}>My Invitation Code</p>
                  <p className={styles.textCode}>X32426</p>
              </Col>
        </Row>
    </Container>
    </div>
  )
}

export default Referral