import SplashScreenPage from "./SplashScreen";
import LoginPage from './Login'
import RegisterPage from './Register'
import HomePage from './Home'
import DepositePage from './Deposite'
import ExchangePage from './Exchange'
import ExchangePageDetail from './Exchange/detail'
import BankPage from './Bank'
import ReferralPage from './Referral'
import WithdrawPage from './Withdraw'
import ChoosePayment from './ChoosePayment'
import ListDeposit from './ListDeposit'
import DepositAmount from './DepositAmount'
import QrDeposit from './QrDeposit'


export {
    SplashScreenPage,
    LoginPage,
    RegisterPage,
    HomePage,
    DepositePage,
    ExchangePage,
    ReferralPage,
    ExchangePageDetail,
    BankPage,
    WithdrawPage,
    ChoosePayment,
    ListDeposit,
    DepositAmount,
    QrDeposit,
}