import React,{useState} from 'react'
import { Container, Image, Row,Col} from 'react-bootstrap'
import styles from './styles.module.css'
import { IconLogoWhite, IconNotif, } from '../../assets'
import { Helmet,  SlideMenu } from '../../components'
import axios from 'axios';
import {  URLKLUWYPAY } from '../../utils/configs'
import { useLocation, useNavigate } from 'react-router-dom'
import QRCode from "react-qr-code";


const QRISDeposit = () => {
  const location = useLocation();
  const [address, setAddress] = useState(location?.state?.address)
    console.log(address)
  return (
    <div className={styles.pages}>
      <Helmet title="QRIS Deposit"/>
       <Container className={styles.containerHeader}>
        <Row className={styles.rowHeader}>
          <Col>
          <SlideMenu/>
          </Col>
          <Col>
            <Image src={IconLogoWhite} className={styles.imgLogo}/>
          </Col>
          <Col style={{textAlign:'right'}}>
          <Image src={IconNotif} className={styles.imgMenu}/>
          </Col>
        </Row>
        {/*  */}
        <Row>
          <Col>
          <p className={styles.TextTitle}>QRIS Deposit</p>
          <p></p>
          </Col>
        </Row>
    </Container>
    <Container className='mt-4'>
      <Row className="text-center">
        <Col>
        <QRCode
              size={256}
              style={{ height: "auto", maxWidth: "50%", width: "50%" }}
              value={address}
              viewBox={`0 0 256 256`}
              />
        </Col>
      </Row>
    </Container>
    </div>
  )
}

export default QRISDeposit