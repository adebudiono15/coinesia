import React from 'react'
import { Container, Image, Row,Col } from 'react-bootstrap'
import styles from './styles.module.css'
import { IconLogoWhite, IconNotif, IconQrCodeBlack} from '../../assets'
import {Helmet, SlideMenu} from '../../components/atoms'
import { useNavigate } from 'react-router-dom'

const Deposite = () => {
  const navigate = useNavigate();

  const choosePayment = () => {
    navigate('/choosePayment')
  }
  return (
    <div className={styles.pages}>
       <Helmet title="Deposit"/>
       <Container className={styles.containerHeader}>
        <Row className={styles.rowHeader}>
          <Col>
          <SlideMenu/>
          </Col>
          <Col>
            <Image src={IconLogoWhite} className={styles.imgLogo}/>
          </Col>
          <Col style={{textAlign:'right'}}>
          <Image src={IconNotif} className={styles.imgMenu}/>
          </Col>
        </Row>
        {/*  */}

        <Row>
          <Col>
          <p className={styles.TextTitle}>Deposit</p>
          <p className={styles.TextSubTitle}>Scan the QR code below.</p>
          </Col>
        </Row>
    </Container>
    <Container className={styles.containerQr}>
      <Row className='text-center'>
              <Col>
                  <Image src={IconQrCodeBlack} className={styles.imgMenuQr} />
                  <div
                  style={{marginTop:20}}
                  />
                  <p className={styles.textAddres}>Deposit Address</p>
                  <p className={styles.textCode}>X32426ghej378jbf8wtJKLM67</p>
              </Col>
        </Row>
    </Container>
    </div>
  )
}

export default Deposite