import React from 'react'
import { Container, Image, Row,Col, Button } from 'react-bootstrap'
import styles from './styles.module.css'
import {BackgroundCard, IconLogoWhite,  IconNotif, IconPlus,IconBitcoin, IconQrCode, Today1, Today2, Today3, IconWithdraw, IconWithdrawHome} from '../../assets'
import { Helmet, SlideMenu } from '../../components/atoms'
import { useNavigate } from 'react-router-dom'

const Home = () =>{
  const navigate = useNavigate();

  const choosePayment = (val) => {
    navigate('/choosePayment',{state: val === 'deposit' ? 'Deposit' : 'Withdraw'})
  }
  
  return (
  <div className={styles.pages}>
     <Helmet title="Home"/>
    <Container className={styles.containerHeader}>
        <Row className={styles.rowHeader}>
          <Col>
          <SlideMenu/>
          </Col>
          <Col>
            <Image src={IconLogoWhite} className={styles.imgLogo}/>
          </Col>
          <Col style={{textAlign:'right'}}>
          <Image src={IconNotif} className={styles.imgMenu}/>
          </Col>
        </Row>
        {/*  */}
        {/* Row Card */}
        <Row className={styles.rowCard}>
          <Col>
            <p className={styles.textTotalWallet}>Total Wallet Balance</p>
            <p className={styles.textWallet}>Rp200,000,000</p>
          </Col>
          <Col style={{placeSelf:'center'}}>
          <Image src={IconPlus} className={styles.imgMenu}/>
          <span className={styles.textAddNewWallet}>Add New Wallet</span>
          </Col>
        </Row>
        {/* Last Row Card */}
    </Container>

    <Container style={{marginTop:200}} className='fluid'>
      {/* Row Card */}
      <Row>
          <div className={styles.wrapperSliderHorizontal}>
          <Col className={styles.colCard}>
            <Row style={{position:'absolute',width:'100%',marginTop:15}}>
                <Col style={{textAlign:'left',marginLeft:20}}>
                <Image width={20} src={IconBitcoin} />
                <div
                style={{marginTop:50}}
                />
                <p className={styles.textBalance}>Balance</p>
                <p className={styles.textPoint}><b>BTC</b> 0.332333</p>
                <p className={styles.textIdr}><b>IDR</b> 35.000.000</p>
                </Col>
                <Col style={{textAlign:'right'}}>
                  <Row>
                    <Col>
                         <Image width={48} src={IconQrCode} />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                    <Button className={styles.btnDeposite} onClick={()=>choosePayment('deposit')}>Deposit</Button>
                    </Col>
                    <Col>
                    <Button className={styles.btnWithdraw} onClick={()=>choosePayment('withdraw')}><Image style={{width:15,height:15}} src={IconWithdrawHome}/></Button>
                    </Col>
                  </Row>
                </Col>
            </Row>
            <Image src={BackgroundCard} className={styles.backgroundCard}/>
          </Col>
        {/*  */}
        <Col className={styles.colCard}>
            <Row style={{position:'absolute',width:'100%',marginTop:15}}>
                <Col style={{textAlign:'left',marginLeft:20}}>
                <Image width={20} src={IconBitcoin} />
                <div
                style={{marginTop:50}}
                />
                <p className={styles.textBalance}>Balance</p>
                <p className={styles.textPoint}><b>BTC</b> 0.332333</p>
                <p className={styles.textIdr}><b>IDR</b> 35.000.000</p>
                </Col>
                <Col style={{textAlign:'right'}}>
                  <Row>
                    <Col>
                         <Image width={48} src={IconQrCode} />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                  <Button className={styles.btnDeposite}>Deposite</Button>
                    </Col>
                  </Row>
                </Col>
            </Row>
            <Image src={BackgroundCard} className={styles.backgroundCard}/>
          </Col>
          {/*  */}
          <Col className={styles.colCard}>
            <Row style={{position:'absolute',width:'100%',marginTop:15}}>
                <Col style={{textAlign:'left',marginLeft:20}}>
                <Image width={20} src={IconBitcoin} />
                <div
                style={{marginTop:50}}
                />
                <p className={styles.textBalance}>Balance</p>
                <p className={styles.textPoint}><b>BTC</b> 0.332333</p>
                <p className={styles.textIdr}><b>IDR</b> 35.000.000</p>
                </Col>
                <Col style={{textAlign:'right'}}>
                  <Row>
                    <Col>
                         <Image width={48} src={IconQrCode} />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                  <Button className={styles.btnDeposite}>Deposite</Button>
                    </Col>
                  </Row>
                </Col>
            </Row>
            <Image src={BackgroundCard} className={styles.backgroundCard}/>
          </Col>
          {/*  */}
          <Col className={styles.colCard}>
            <Row style={{position:'absolute',width:'100%',marginTop:15}}>
                <Col style={{textAlign:'left',marginLeft:20}}>
                <Image width={20} src={IconBitcoin} />
                <div
                style={{marginTop:50}}
                />
                <p className={styles.textBalance}>Balance</p>
                <p className={styles.textPoint}><b>BTC</b> 0.332333</p>
                <p className={styles.textIdr}><b>IDR</b> 35.000.000</p>
                </Col>
                <Col style={{textAlign:'right'}}>
                  <Row>
                    <Col>
                         <Image width={48} src={IconQrCode} />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                  <Button className={styles.btnDeposite}>Deposite</Button>
                    </Col>
                  </Row>
                </Col>
            </Row>
            <Image src={BackgroundCard} className={styles.backgroundCard}/>
          </Col>
          {/*  */}
          <Col className={styles.colCard}>
            <Row style={{position:'absolute',width:'100%',marginTop:15}}>
                <Col style={{textAlign:'left',marginLeft:20}}>
                <Image width={20} src={IconBitcoin} />
                <div
                style={{marginTop:50}}
                />
                <p className={styles.textBalance}>Balance</p>
                <p className={styles.textPoint}><b>BTC</b> 0.332333</p>
                <p className={styles.textIdr}><b>IDR</b> 35.000.000</p>
                </Col>
                <Col style={{textAlign:'right'}}>
                  <Row>
                    <Col>
                         <Image width={48} src={IconQrCode} />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                  <Button className={styles.btnDeposite}>Deposite</Button>
                    </Col>
                  </Row>
                </Col>
            </Row>
            <Image src={BackgroundCard} className={styles.backgroundCard}/>
          </Col>
          </div>
        </Row>
        {/* Last Row Card */}
    </Container>

    <Container className={styles.containerBottom}>
        <Row> 
          <Col>
          <p className={styles.textActivity}>Activities</p>
            <hr style={{width:'100%',position:'absolute',left:0,marginTop:-10}}/>
          </Col>
        </Row>
        {/* <div className={styles.wrapperSliderVertical}>
        <Row>
          <Col>
          <p className={styles.textToday}>Today</p>
          </Col>
        </Row>
        <hr style={{marginTop:-5}}/>
        <Row>
          <Col lg={2}>
            <Image src={Today1} width={34}/>
            </Col>
            <Col>
            <p className={styles.textTime}>12:21 PM <br></br>
            Exchange USD to IDR</p>
            </Col>
            <Col>
            <p className={styles.textDanger}>-50</p>
            </Col>
        </Row>
        <hr style={{marginTop:-5}}/>
        <Row>
          <Col lg={2}>
            <Image src={Today2} width={34}/>
            </Col>
            <Col>
            <p className={styles.textTime}>12:21 PM <br></br>
            Exchange USD to IDR</p>
            </Col>
            <Col>
            <p className={styles.textDanger}>-50</p>
            </Col>
        </Row>
        <hr style={{marginTop:-5}}/>
        <Row>
          <Col lg={2}>
            <Image src={Today1} width={34}/>
            </Col>
            <Col>
            <p className={styles.textTime}>12:21 PM <br></br>
            Exchange USD to IDR</p>
            </Col>
            <Col>
            <p className={styles.textDanger}>-50</p>
            </Col>
        </Row>
        <hr style={{marginTop:-5}}/>
        <Row>
          <Col lg={2}>
            <Image src={Today3} width={34}/>
            </Col>
            <Col>
            <p className={styles.textTime}>12:21 PM <br></br>
            Exchange USD to IDR</p>
            </Col>
            <Col>
            <p className={styles.textDanger}>-50</p>
            </Col>
        </Row>
        </div> */}
    </Container>
 
    </div>
  )
}

export default Home