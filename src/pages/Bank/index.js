import React,{useState,useEffect} from 'react'
import { Container, Image, Row,Col,InputGroup,Form,DropdownButton,Dropdown,Button,FormControl } from 'react-bootstrap'
import styles from './styles.module.css'
import { IconLogoWhite, IconNotif, IconQrCodeBlack,IconFlag, IconExchange, iconSearch, IconBca, IconBri, IconBni, IconMandiri} from '../../assets'
import { Helmet, Required, SlideMenu } from '../../components'
import axios from 'axios';
import {   Messaege } from '../../utils/helper'
import { URLAPI } from '../../utils/configs'
import Select from 'react-dropdown-select';
import { useNavigate } from 'react-router-dom'
import AsyncStorage from '@react-native-async-storage/async-storage';


const Bank = () => {
    const [dataBank, setDataBank] = useState([]);
    const [selectBank, setSelectBank] = useState('')
    const [accountNumber, setAccountNumber] = useState('')
    const [otpNumber, setOtpNumber] = useState('')
    const [bank, setBank] = useState([])
    const [bankUser, setBankUser] = useState([])
    const [user,setUser] = useState([])

    // Bank user
    const getBankUser = async () => {
      axios({
        url: URLAPI +`/bank-user/`+ await AsyncStorage.getItem('username'),
        method: 'get',
        headers: {
          Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        })
        .then(response => {
          setBankUser(response.data.data)
          console.log(response.data.data)
        })
        .catch(error => {
          console.log(error)
            
        });
    }

     const getBank = async () => {
      axios({
        url: URLAPI +'/bank',
        method: 'get',
        headers: {
          Authorization: 'Bearer ' + await AsyncStorage.getItem('token'),
        },
        })
        .then(response => {
            setDataBank(response.data)
        })
        .catch(error => {
          console.log(error)
            
        });
    }

    const items = dataBank.map(item => {
        const data = {};
        data.label = item.nama;
        data.value = item.nama;
        return data;
      });

    useEffect(() => {
        getBank();
        getBankUser();
    }, [])

    useEffect(() => {
        async function fetchData() {
          try {
            const savedUser = await AsyncStorage.getItem("user");
            const currentUser = JSON.parse(savedUser);
            setUser(currentUser);
          } catch (error) {
            console.log(error);
          }
        }
        fetchData();
      }, [])


    const handlePost = async () => {
      if(otpNumber == 111)
      {
        axios({
          url: URLAPI +'/bank-user',
          method: 'post',
          headers: {
            Authorization: 'Bearer ' + (await AsyncStorage.getItem('token')),
          },
          data: {
              customer: user.username,
              name: selectBank,
              number_account: accountNumber
          },
          })
          .then(response => {
              setAccountNumber('')
              setOtpNumber('')
              Messaege('Succes','Successfully added data','success');
              getBankUser();
          })
          .catch(error => {
              Messaege('Failed','Make sure everything is filled','error');
          });
      }else{
        Messaege('Failed','OTP Wrong','error');
      }
        
    }



  return (
    <div className={styles.pages}>
      <Helmet title="Bank"/>
       <Container className={styles.containerHeader}>
        <Row className={styles.rowHeader}>
          <Col>
          <SlideMenu/>
          </Col>
          <Col>
            <Image src={IconLogoWhite} className={styles.imgLogo}/>
          </Col>
          <Col style={{textAlign:'right'}}>
          <Image src={IconNotif} className={styles.imgMenu}/>
          </Col>
        </Row>
        {/*  */}

        <Row>
          <Col>
          <p className={styles.TextTitle}>Bank</p>
          <p className={styles.TextSubTitle}>Add and save your favorite bank accounts here.</p>
          </Col>
        </Row>
        <Row>
            <Col>
                <InputGroup className="mb-4">
                    <FormControl className={styles.formSearch} placeholder='Search Banks' />
                    <InputGroup.Text className={styles.borderButton}>
                        <Image src={iconSearch} width={15} height={15} />
                    </InputGroup.Text>
                </InputGroup>
           </Col>
        </Row>
    </Container>
    <Container className={styles.containerQr}>
      <Row>
      <Col>
      <p className={styles.textCode}>My List Bank</p>
      <hr/>
      <Row>
          <Col lg={6} md={6} sm={6} className={styles.textCode}>Name </Col>
          <Col className={styles.textCode}>Account No</Col>
        </Row>
      <Row className='mt-3 mb-3' style={{maxHeight:200,overflowX:'auto'}}>
        <Col>
        <hr/>
        {bankUser?.map((i,idx)=> {
          return (
            <>
            <Row>
              <Col><span className={styles.textCode}>{i?.name}</span></Col>
              <Col><span className={styles.textCode}>{i?.number_account}</span></Col>
            </Row>
            <hr/>
            </>
          )
        })}
        </Col>
      </Row>
      </Col>
      </Row>
        {/*  */}
        <Row>
            <Col>
            <p className={styles.textCode}>Add New Bank Account</p>
            <p className={styles.textAddres}>Choose and enter your bank account number.</p>
            <div style={{height:10}}/>
            <Select
            placeholder="Select Bank"
            className={styles.formSearch}
            options={items}
            hideSelectedOptions={false}
            value={bank}
            onChange={(selected) => setSelectBank(selected[0].value)}
            />
            </Col>
        </Row>
        <Row>
            <Col>
            <Form.Label className={styles.labelForm}>Account Number<Required/></Form.Label>
            <FormControl className={styles.formSearch} placeholder='Account Number' name="accountNumber" value={accountNumber}   onChange={e => setAccountNumber(e.target.value)} />
            </Col>
        </Row>
        <Row className='mt-3'>
            <Form.Label className={styles.labelForm}>OTP {otpNumber}<Required/></Form.Label>
            <Col lg={6}>
            <FormControl className={styles.formSearch} placeholder='OTP Number' name="otpNumber" value={otpNumber}   onChange={e => setOtpNumber(e.target.value)} />
            </Col>
            <Col lg={6}>
            <p className={styles.textCodeOTP}>Send OTP</p>
            </Col>
        </Row>
        <Row>
            <Col>
            <Button className={styles.btnProceed} onClick={handlePost}>Add</Button>
            </Col>
        </Row>
    </Container>
    </div>
  )
}

export default Bank